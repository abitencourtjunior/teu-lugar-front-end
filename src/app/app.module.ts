import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SignInComponent} from './components/authentication/sign-in/sign-in.component';
import {AppBarComponent} from './components/commons/app-bar/app-bar.component';

import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppMaterialModule} from './app.material';
import {FooterComponent} from './components/commons/footer/footer.component';
import {FormsModule} from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { JwtModule } from "@auth0/angular-jwt";
import { UserComponent } from './components/user/user.component';
import { CompanyComponent } from './components/company/company.component';
import { AccessComponent } from './components/access/access.component';

function getToken() {
  return sessionStorage.getItem('chaveSegura')
}

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    AppBarComponent,
    FooterComponent,
    HomeComponent,
    UserComponent,
    CompanyComponent,
    AccessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    FormsModule,
    JwtModule.forRoot({ config: { tokenGetter: getToken, 
      whitelistedDomains: ["192.168.6.104:8080", "localhost:8s080"] } })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
