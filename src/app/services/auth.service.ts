import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  public signIn(data): Observable<any> {
    return this.httpClient.post(`${environment.api}/login`, data, {observe: 'response', responseType: 'text'});
  }

  public createUser(data): Observable<any> {
    return this.httpClient.post(`${environment.api}/user`, data, {observe: 'response'});
  }

  public static getToken() {
    return sessionStorage.getItem('chaveSegura')
  }
}
