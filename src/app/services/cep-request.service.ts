import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class CepRequestService {

  constructor(private http: HttpClient) {
  }

  getCep(): Observable<any> {
    return this.http.get('https://viacep.com.br/ws/01001000/json/');
  }
}
