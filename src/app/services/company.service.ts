import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private httpClient: HttpClient) { }


  public createCompany(data): Observable<any> {
    return this.httpClient.post(`${environment.api}/company`, data, {observe: 'response'});
  }

}
