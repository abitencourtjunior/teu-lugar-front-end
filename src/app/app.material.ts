import {NgModule} from '@angular/core';
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatToolbarModule, MatGridListModule, MatMenuModule, MatIconModule, MatCardModule} from '@angular/material';

@NgModule({
  imports: [MatToolbarModule, MatInputModule, MatFormFieldModule, 
    MatButtonModule, MatGridListModule, MatMenuModule, MatIconModule, MatCardModule],
  exports: [MatToolbarModule, MatInputModule, MatFormFieldModule, 
    MatButtonModule, MatGridListModule, MatMenuModule, MatIconModule, MatCardModule], 
})

export class AppMaterialModule {
}
