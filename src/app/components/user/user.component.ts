import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private authService: AuthService, 
    private router: Router) {
}
ngOnInit() {
}


async createUser(form){
  try {
    const {status, body} = await this.authService.createUser(form.value).toPromise();
    if(status === 200){
      this.router.navigate(['/home'])
    }
  } catch (e) {
    console.error(e);
  }
}
}
