import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  
  constructor(private authService: AuthService, 
              private router: Router) {
  }

  ngOnInit() {
  }

  async signIn(form) {
    try {
      const response = await this.authService.signIn(form.value).toPromise();
      const { status, body } = response;
      if (!body) return console.log('ErroMuitoLoko'); 
      if (status === 200) {
        sessionStorage.setItem('chaveSegura', body.split(" ")[1]);
        this.router.navigate(['/home'])
      }
    } catch (e) {
      console.error(e);
    }
  }

  async createUser(form){
    try {
      const {status, body} = await this.authService.createUser(form.value).toPromise();
    } catch (e) {
      console.error(e);
    }
  }
}
