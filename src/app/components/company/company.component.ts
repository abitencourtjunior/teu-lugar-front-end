import { Component, OnInit } from '@angular/core';
import { CompanyService } from 'src/app/services/company.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  constructor(private companyService: CompanyService) { }

  ngOnInit() {
  }

  async createCompany(form) {
    try {
      const response = await this.companyService.createCompany(form.value).toPromise();
      const { status, body } = response;
      if (!body) return console.log('ErroMuitoLoko -> ' + body); 
      if (status === 200) {
        console.log(body)
      }
    } catch (e) {
      console.error(e);
    }
  }
}
